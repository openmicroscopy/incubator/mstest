#!/usr/bin/env python
import requests
import re
from pprint import pprint
from argparse import ArgumentParser
from jsondiff import diff
from sys import exit
from os import environ


def chk(response):
    if response.status_code != 200:
        raise Exception('{}: {}'.format(
            response.status_code, response.reason))
    return response


def data(**kwargs):
    rv = {'experimenter_id': -1, 'group': 3, 'page': 1}
    rv.update(kwargs)
    return rv


calls = dict()
calls["gene"] = (
    ("mapr/api/autocomplete/gene/", data(value="ash2", query=True, case_sensitive=False)),
    ("mapr/api/gene/count/", data()),
    #("mapr/api/gene/", data(orphaned=True)),
    ("mapr/api/gene/count/", data(value="ash2", case_sensitive=False)),
    #("mapr/api/gene/", data(value="ash2", case_sensitive=False, orphaned=True)),
    #("mapr/api/gene/", data(value="ash2", case_sensitive=False, counter=65, id="ash2", page=0)),  # 0 ?!
    ("mapr/api/gene/annotations/", data(type="map", map="ash2")),
    ("mapr/api/gene/plates/", data(value="ash2", case_sensitive=False, counter=60, id=3)),
    #("mapr/api/gene/images/", data(value="ash2", case_sensitive=False, node="plate", id=2568)),
)

calls["organism"] = (
    ("mapr/api/organism/count/", data()),
    #("mapr/api/organism/", data(orphaned=True)),
    ("mapr/api/autocomplete/organism/", data(value="Homo sapiens", query=True, case_sensitive=False)),
    ("mapr/api/organism/count/", data(value="Homo sapiens", case_sensitive=False)),
    #("mapr/api/organism/", data(value="Homo sapiens", case_sensitive=False, orphaned=True)),
    #("mapr/api/organism/", data(value="Homo sapiens", case_sensitive=False, counter=65, id="Homo sapiens", page=0)),  # 0 ?!
    ("mapr/api/organism/annotations/", data(type="map", map="Homo sapiens")),
    ("mapr/api/organism/plates/", data(value="Homo sapiens", case_sensitive=False, counter=60, id=3)),
    #("mapr/api/organism/images/", data(value="Homo sapiens", case_sensitive=False, node="plate", id=2568)),
)

class Base(object):

    def __init__(self, ns):
        self._user = ns.name
        self._pass = ns.password
        self._timeout= ns.timeout
        self.session = requests.session()
        self.prefix = ""

    def get(self, path, data, **kwargs):
        if "timeout" not in kwargs:
            kwargs["timeout"] = self._timeout
        url = self.server
        if self.prefix:
            url += self.prefix + "/"
        url += path
        return self.session.get(url, params=data, **kwargs)

    def close(self):
        self.session.close()


class VI(Base):

    def __init__(self, ns, server="http://idr-experimental.openmicroscopy.org/"):
        Base.__init__(self, ns)
        self.server = server


class V0(Base):

    def __init__(self, ns, server="https://web-proxy.openmicroscopy.org/ms/", prefix="v0"):
        Base.__init__(self, ns)
        self.server = server
        self.prefix = prefix
        login_url = "/".join([server, prefix, 'webclient/login/'])
        response = chk(self.session.get(login_url))
        csrftoken = self.session.cookies['csrftoken']
        data = {'username': self._user, 'password': self._pass,
            'csrfmiddlewaretoken': csrftoken,
            'server': 1, 'noredirect': 1,
        }
        response = chk(self.session.post(login_url, data=data, headers={'Referer': login_url}))


class V12(Base):

    def __init__(self, ns, server="https://web-proxy.openmicroscopy.org/ms/", prefix="v1"):
        Base.__init__(self, ns)
        self.server = server
        self.prefix = prefix
        login_url = server + 'login'
        if not ns.nologin:
            data = {'username': ns.name, 'password': ns.password}
            response = chk(self.session.post(login_url, json=data))


def matches_filter(regexes, test):
    if not regexes: 
        return True

    found = False
    for r in regexes:
        if re.match(r, test):
            found = True
            break
    return found


def main(ns):
    versions = dict()
    if matches_filter(ns.impl, "__"):
        versions["__"] = VI(ns)
    if matches_filter(ns.impl, "v0"):
        versions["v0"] = V0(ns)
    if matches_filter(ns.impl, "v1"):
        versions["v1"] = V12(ns)
    if matches_filter(ns.impl, "v2"):
        versions["v2"] = V12(ns, prefix="v2")
    try:
        for path, data in calls.get(ns.type, ()):
            if not matches_filter(ns.path, path):
                continue

 
            print path
            results = []  
            for k, v in sorted(versions.items()):
                print "\t", k,
                try:
                    rsp = v.get(path, data)
                    print rsp.elapsed, rsp.status_code, rsp.status_code == 200 and "" or rsp.reason,
                    json = rsp.json()
                    results.append(json)
                    if len(results) == 1:
                        print "size:", len(rsp.content),
                        if ns.pprint:
                            print
                            pprint(json)
                        else:
                            print str(json)[0:ns.width]
                    else:
                        _diff = diff(results[0], results[-1])
                        if _diff:
                            print "but differs:", _diff
                        else:
                            print "!"
                        if ns.pprint:
                            print
                            pprint(json)
                            pprint(_diff)
                except requests.exceptions.ReadTimeout as rt:
                    print "TIMEOUT"
                except Exception as e:
                    print "FAIL", type(e), e
    
            
    finally:
        for v in versions.values():
            v.close()

if __name__ == "__main__":
    parser = ArgumentParser("test.py")
    parser.add_argument("--name", default="public")
    parser.add_argument("--password", default="public")
    parser.add_argument("--type", default="gene")
    parser.add_argument("--timeout", type=int, default=15)
    parser.add_argument("--width", type=int, default=80)
    parser.add_argument("--pprint", action="store_true")
    parser.add_argument("--nologin", action="store_true")
    parser.add_argument("--impl", nargs="*", help="implementation regex, e.g. '[12]'", default=('.*[_12].*',))
    parser.add_argument("path", nargs="*", help="path regex, e.g. '.*annotations.*'")
    ns = parser.parse_args()
    main(ns)
